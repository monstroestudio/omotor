let gulp = require('gulp');
let jshint = require('gulp-jshint');
let concat = require('gulp-concat');
let uglify = require('gulp-uglify');
let sass = require('gulp-sass');
let rename = require('gulp-rename');
let cleanCSS = require('gulp-clean-css');
let watch = require('gulp-watch');

// CSS
gulp.task('css-vendor', function() {
    return gulp.src([
            'node_modules/bootstrap/dist/css/bootstrap.min.css',
            'node_modules/slick-carousel/slick/slick.css',
            'node_modules/slick-carousel/slick/slick-theme.css',
            'node_modules/selectric/public/selectric.css'
        ])
        .pipe(concat('vendor.css'))
        .pipe(gulp.dest('./dist/css/'));
});

gulp.task('css-main', function() {
    return gulp.src('src/sass/base.scss')
        .pipe(sass({ errLogToConsole: true }))
        .pipe(rename('style.css'))
        .pipe(cleanCSS())
        .pipe(gulp.dest('./dist/css/'));
});

// Scripts
gulp.task('jshint', function() {
    return gulp.src('src/js/**/*.js')
        .pipe(jshint())
        .pipe(jshint.reporter('default'));
});

gulp.task('js-vendor', function () {
    return gulp.src([
            'node_modules/jquery/dist/jquery.min.js',
            'node_modules/bootstrap/dist/js/bootstrap.min.js',
            'node_modules/moment/min/moment-with-locales.min.js',
            'node_modules/slick-carousel/slick/slick.min.js',
            'node_modules/selectric/public/jquery.selectric.min.js',
            'node_modules/jquery-mask-plugin/dist/jquery.mask.min.js'
        ])
        .pipe(concat('vendor.js'))
        .pipe(gulp.dest('./dist/js/'));
});

gulp.task('js-main', gulp.series('jshint', function () {
    return gulp.src('src/js/**/*.js')
        .pipe(uglify())   
        .pipe(concat('main.js'))
        .pipe(gulp.dest('./dist/js/'));
}));

// Observer
gulp.task('watch', function() {
    gulp.watch('src/sass/*.scss', gulp.series('css-main'));
    gulp.watch('src/js/**.js', gulp.series('js-main'));
});

// Default
gulp.task('default', gulp.series('js-vendor', 'js-main', 'css-vendor', 'css-main', 'watch'));