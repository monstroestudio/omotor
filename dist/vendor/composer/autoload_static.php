<?php

// autoload_static.php @generated by Composer

namespace Composer\Autoload;

class ComposerStaticInitc81a03db406d1142fe97539c32e9e36f
{
    public static $prefixLengthsPsr4 = array (
        'P' => 
        array (
            'PHPMailer\\PHPMailer\\' => 20,
        ),
    );

    public static $prefixDirsPsr4 = array (
        'PHPMailer\\PHPMailer\\' => 
        array (
            0 => __DIR__ . '/..' . '/phpmailer/phpmailer/src',
        ),
    );

    public static function getInitializer(ClassLoader $loader)
    {
        return \Closure::bind(function () use ($loader) {
            $loader->prefixLengthsPsr4 = ComposerStaticInitc81a03db406d1142fe97539c32e9e36f::$prefixLengthsPsr4;
            $loader->prefixDirsPsr4 = ComposerStaticInitc81a03db406d1142fe97539c32e9e36f::$prefixDirsPsr4;

        }, null, ClassLoader::class);
    }
}
