<?php
// Import PHPMailer classes into the global namespace
// These must be at the top of your script, not inside a function
use PHPMailer\PHPMailer\PHPMailer;
use PHPMailer\PHPMailer\Exception;

//Load Composer's autoloader
require 'vendor/autoload.php';

$mail = new PHPMailer;

$mail->IsSMTP();                                      // Set mailer to use SMTP
$mail->Host = 'smtp.mailgun.org';                 // Specify main and backup server
$mail->Port = 587;                                    // Set the SMTP port
$mail->SMTPAuth = true;                               // Enable SMTP authentication
$mail->Username = 'postmaster@monstroestudio.com.br';                // SMTP username
$mail->Password = 'e4cfc5d43b57cc0cdca3922e90925785-4836d8f5-05fa9722';                  // SMTP password
$mail->SMTPSecure = 'tls';                            // Enable encryption, 'ssl' also accepted

$mail->From = 'noreply@monstroestudio.com.br.com.br';
$mail->FromName = 'OMotor';
$mail->AddAddress('marcio@omotor.com.br', 'Marcio');  // Add a recipient

$mail->IsHTML(true);                                  // Set email format to HTML

$mail->Subject = 'Trabalhe Conosco - Questionario';
$mail->Body    = "
    <p><strong>Nome: </strong> {$_POST['name']}</p>
    <p><strong>E-mail: </strong> {$_POST['email']}</p>
    <p><strong>Empresa: </strong> {$_POST['company']}</p>
    <p><strong>Telefone: </strong> {$_POST['phone']}</p>
    <hr>
    <p><strong>Mensagem </strong><br>{$_POST['msg']}</p>
";

if(!$mail->Send()) {
   echo 'Message could not be sent.';
   echo 'Mailer Error: ' . $mail->ErrorInfo;
   exit;
}

echo 'Message has been sent';